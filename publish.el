;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;; package install must be run beforehand

;;; Code:

(require 'package)
(package-initialize)

(require 'org)
;;(require 'ox-publish)
(require 'ox-hugo)

(setq org-confirm-babel-evaluate nil
      org-src-fontify-natively t
      org-src-tab-acts-natively t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
  (shell . t)
  (sqlite . t)
  ))

(setq org-table-convert-region-max-lines 9999)
;; setting to nil, avoids "Author: x" at the bottom
;; (setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

;; (setq org-html-divs '((preamble "header" "top")
;;                       (content "main" "content")
;;                       (postamble "footer" "postamble"))
;;       org-html-container-element "section"
;;       org-html-metadata-timestamp-format "%Y-%m-%d"
;;       org-html-checkbox-type 'html
;;       org-html-html5-fancy t
;;       org-html-validation-link nil
;;       org-html-doctype "html5")

;; (defvar site-attachments
;;   (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
;;                 "ico" "cur" "css" "js" "woff" "html" "pdf"))
;;   "File types that are published as static files.")

;; (setq org-publish-project-alist
;;       (list
;;        (list "site-org"
;;              :base-directory "."
;;              :base-extension "org"
;;              :recursive t
;;              :publishing-function '(org-hugo-export-to-md)
;;              :publishing-directory "./public"
;;              :exclude (regexp-opt '("README" "draft" "*scratch*"))
;;              :auto-sitemap t
;;              :sitemap-filename "index.org"
;;              :sitemap-file-entry-format "%d *%t*"
;;              :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>"
;;              :sitemap-style 'list
;;              :sitemap-sort-files 'anti-chronologically)
;;        (list "site-static"
;;              :base-directory "."
;;              :exclude "public/"
;;              :base-extension site-attachments
;;              :publishing-directory "./public"
;;              :publishing-function 'org-publish-attachment
;;              :recursive t)
;;        (list "site" :components '("site-org"))))

(provide 'publish)
;;; publish.el ends here
