#+TITLE: Refile
#+hugo_base_dir: ./hugo_base_dir
#+HUGO_SECTION: post
#+author:
#+FILETAGS: REFILE


* Babel shell script  
During a gitlab job it is possible to run code of many different languages in an org-babel source block and use or display results here through exporting to Hugo each run.

A simple example is to display the current working directory using in the gitlab-runner.

#+begin_src sh :exports both :results drawer
pwd
#+end_src

Export to a list which show as bullets in hugo.
#+begin_src sh :exports both :colnames no :results drawer output list
ls
#+end_src

** Results to tables

Tables possible with SQL or python and others also

#+name: ls_l_to_table
#+begin_src sh :exports both :results drawer output table 
(printf "PERM LINKS OWNER GROUP SIZE MONTH DAY " ; \
           printf "HH:MM/YEAR NAME\n" ; \
           ls -l | sed 1d) | column -t
#+end_src

** bring org-table results into new source block for a line count
Here's an example from an [[http://eschulte.github.io/org-scraps/scraps/2011-01-18-passing-arguments-to-the-shell.html][org-scrap]]

#+begin_src sh :var data=ls_l_to_table :exports both :results drawer 
  echo "$data"|wc -l
#+end_src


* Look at Processes

#+begin_src sh :exports both
ps | awk '{print $0}' 
#+end_src


* Deeper look

** Hitting 1000 rows
executing Sh code block (ls_Ral_to_table)...
Error reading results: (user-error Region is longer than ‘org-table-convert-region-max-lines’ (999) lines; not converting)


Set org-table-convert-region-max-lines to a high value if nessecary.

#+begin_src emacs-lisp  :exports both 

(print org-table-convert-region-max-lines)
#+end_src

** ls -Ral
list all directories and files recursively from the current directory 
With the help of [[https://stackoverflow.com/questions/1767384/ls-command-how-can-i-get-a-recursive-full-path-listing-one-line-per-file][ls-command-how-can-i-get-a-recursive-full-path-listing-one-line-per-file]]
I've adopted the ls to table command to store more values than I want to display.

#+name: ls_Ral_to_table
#+begin_src sh :exports code :results drawer table
(printf "DIR PERM LINKS OWNER GROUP SIZE MONTH DAY " ; \
           printf "HH:MM/YEAR NAME\n" ; \
	     ls -Ral . | awk '	     /:$/&&f{s=$0;f=0} 
	     /:$/&&!f{sub(/:$/,"");s=$0;f=1;next} 
	     NF&&f{ print s"/ "$0 }' | sed 1d) | column -t
#+end_src

** SQL also

[[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-sqlite.html][ob-sqlite]]

#+begin_src sqlite :db /tmp/rip.db :var orgtable=ls_Ral_to_table :exports both :results drawer
drop table if exists testtable;
.mode csv testtable
.import $orgtable testtable
	select count(*) from testtable;
#+end_src

Have a look at the column names.
#+begin_src sqlite :db /tmp/rip.db :var orgtable=ls_Ral_to_table :exports both :results drawer code
.schema
#+end_src

List the 10 largest and include a row with column names on top of it for hugo

#+name: top_10_files
#+begin_src sqlite :db /tmp/rip.db :var orgtable=ls_Ral_to_table :exports both
SELECT 'Name','Size','Day',' Month', 'Directory' UNION ALL
SELECT * FROM 	
 (SELECT  name, size,day,month,dir FROM testtable 
 ORDER BY CAST(size AS INT) DESC LIMIT 10);
#+end_src
