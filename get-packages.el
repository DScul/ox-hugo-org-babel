;; get-packages.el --- Publish org-mode project on Gitlab Pages

;;; Commentary:
;; This script will get the extra packages
;; 
;;; Code:

(require 'package)
(package-initialize)

;; Can avoid connecting if we check we have the packages but not nessecary
(unless (and ;;(package-installed-p 'org-plus-contrib)
	     (package-installed-p 'ox-hugo)
    	     (package-installed-p 'htmlize)
	     )
  
    (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
    (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
    (package-refresh-contents)

    ;;(package-install 'org-plus-contrib)
    (package-install 'ox-hugo)
    (package-install 'htmlize)
)
(provide 'get-packages)
;;; get-packages ends here
